# Sample database: http://beta.json-generator.com/api/json/get/N1uaMkIde

# Find and Replace ionSocialApp with your own value
# To generate the data we use this tool: http://www.json-generator.com/

# You can find the documentation here: http://bit.ly/ionicthemes-ionSocialApp

## To save all installed plugins to config.xml run
```
cordova plugin save
```

## To install platforms and plugins run:
```
ionic state restore
```


SET UP
First you will need to have Ionic installed in your environment. Please refer to the Ionic offical installation documentation in order to install Ionic and cordova: http://ionicframework.com/docs/guide/installation.html

Run npm install in order to install all the node modules required by the app.
Run bower install in order to install all the javascript libraries required by the app.
This project uses Sass so make sure you setup your project to use Sass by running ionic setup sass.

To test the app please refer to this guide: http://ionicframework.com/docs/guide/testing.html


Note: If you are experiencing that your app doesn’t refresh automatically when you update your html views, that’s probably because we are using angular templatecache to preprocess all the views in the app which improve performance dramatically. Also see gulp-angular-templatecache.

Let us explain this a little bit. If we choose to follow the “default” angular/ionic approach we would have each of our views loaded with a particular AJAX request. If we have many views, this will cause many AJAX request slowing down the app needlessly. The alternative we use is using angular templatecache  which concatenates all the views html into one javascript file that loads those views to angular avoiding multiple AJAX requests. With this approach we just need to make one request to that javascript file (named app.views.js).
We use gulp tasks that do this automatically, so when you change your html in your text editor gulp will re-create this app.views.js file and all will be ok. 

The problem may occur when you don’t have gulp running and you are changing your html, in this case gulp won’t be able to re-create app.views.js file so you won’t see your html updates in your app.
You can fix this by turning on gulp (by running ionic serve) or by removing the dependencies to app.views.js file and your_app_name.views module in your app.js (this will cause you to follow the angular default approach).
