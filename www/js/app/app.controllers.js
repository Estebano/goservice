angular.module('goService.app.controllers',
    [
        'goService.app.menuController',
        'goService.app.bookingsController',
        'goService.app.mapController',
        'goService.app.workersController',
        'goService.app.settingsControllers',
        'goService.app.MessagesController',
        'goService.app.notificationsController',
        'goService.app.bookAppointmentController',
        'goService.app.feedsController'
    ])

    .controller('ProfileCtrl', function ($scope, $ionicHistory, $state, $stateParams, $ionicScrollDelegate, $ionicLoading, $ionicPopup, User, pictures, $ionicModal, CategoryList, AuthService, WorkerService, FeedService, Error, Upload, APP_CONSTANT) {
        $scope.$on('$ionicView.afterEnter', function () {
            $ionicScrollDelegate.$getByHandle('profile-scroll').resize();

            if (typeof User.get('id') === 'undefined') {
                $state.go('welcome-back');
                User.set('');
            }

        });

        $scope.currentUserId = $stateParams.userId;
        $scope.user = null;
        $scope.worker = null;
        $scope.me = User.get();
        $scope.progressPercentage = 0;
        $scope.uploadProgress = {'width': '0%'};
        $scope.file = null;


        $ionicLoading.show({
            template: 'Getting profile...'
        });

        var getUser_promise = AuthService.getUser($scope.currentUserId);
        getUser_promise.then(function (user) {
            $ionicLoading.hide();

            if (typeof user.data.data[0] === 'undefined') $state.go('app.workers');
            $scope.user = user.data.data[0];
            $scope.myProfile = $scope.me.id == $scope.user.id;

            profileSecurity();

            if (!$scope.user.worker && $scope.myProfile) setRegisterWorkerForm();
            if ($scope.user.worker) getWorker($scope.user.id);

            if (!$scope.myProfile) loadAppointmentModal();

            getPost($scope.user.id);

            setProfileData();


        }, function (reason) {
            Error.show(reason);
            profileSecurity();
        });

        var profileSecurity = function () {
            //prevent accessing to a profile that is not worker and not Me
            if ($scope.user == null || (!$scope.user.worker && !$scope.myProfile)) {
                $state.go('app.profile.posts', {userId: $scope.me.id}, {reload: true});
                return;
            }
        };

        var loadAppointmentModal = function () {
            $ionicModal.fromTemplateUrl('views/app/bookings/book_appointment.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.appointment_modal = modal;
            });
        };

        var getWorker = function (userId) {
            $ionicLoading.show({
                template: 'Setting profile...'
            });
            var getWorker_promise = AuthService.getWorker(userId);
            getWorker_promise.then(function (worker) {
                $ionicLoading.hide();
                $scope.worker = worker.data.data[0];
                User.setKey('worker_info', $scope.worker);


            }, function (reason) {
                Error.show(reason);
            });
        };

        var getPost = function (userId) {
            $ionicLoading.show({
                template: 'Getting your ads...'
            });
            FeedService.getMyPosts(userId).then(function (posts) {
                $ionicLoading.hide();
                $scope.user.posts = posts.data.data;
            }, function (reason) {
                Error.show(reason);
            });
        };

        var setProfileData = function () {
            $scope.user.pictures = pictures;
        };

        var setRegisterWorkerForm = function () {
            $scope.categories = CategoryList.get();
            $scope.me.category = "";

            $ionicModal.fromTemplateUrl('views/app/profile/register-worker-form.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.register_worker_modal = modal;
            });
        };

        $scope.setVisibleWorker = function () {

            $ionicLoading.show({
                template: 'Setting visibility...'
            });

            var setWorkerVisibility_promise = AuthService.updateWorker({visible: $scope.worker.visible}, $scope.worker.id);
            setWorkerVisibility_promise.then(function (worker) {

                $scope.worker = worker.data.data[0];
                User.setKey('worker_visible', $scope.worker.visible);
                $ionicLoading.hide();

            }, function (reason) {
                Error.show(reason);
            });

        };

        //todo portfolio list
        $scope.getUserPics = function () {
            //we need to do this in order to prevent the back to change
            $ionicHistory.currentView($ionicHistory.backView());
            $ionicHistory.nextViewOptions({disableAnimate: true});
            $state.go('app.profile.pics', {userId: $scope.user.id});
        };

        $scope.getUserPosts = function () {
            //we need to do this in order to prevent the back to change
            $ionicHistory.currentView($ionicHistory.backView());
            $ionicHistory.nextViewOptions({disableAnimate: true});
            $state.go('app.profile.posts', {userId: $scope.user.id});
        };

        $scope.registerWorker = function () {
            var address = $scope.me.postal_code + " " + $scope.me.state;
            WorkerService.getLatLng(address).then(function (location) {

                var request_data = {
                    user_id: $scope.me.id,
                    hourly_rate: $scope.me.hourly_rate,
                    about: $scope.me.about,
                    category: $scope.me.category,
                    cover: $scope.me.cover,
                    lat: location.data.results[0].geometry.location.lat,
                    lng: location.data.results[0].geometry.location.lng,
                    visible: true
                };

                $ionicLoading.show({
                    template: 'Registering...'
                });

                var registerWorker_promise = AuthService.registerWorker(request_data);
                registerWorker_promise.then(function (worker) {
                    $scope.worker = worker.data.data[0];
                    $ionicLoading.hide();

                    $scope.updateUser();

                }, function (reason) {
                    Error.show(reason);
                });

            });
        };

        $scope.updateUser = function () {
            $ionicLoading.show({
                template: 'Updating status...'
            });
            var registerWorker_promise = AuthService.updateUser({worker: true}, $scope.me.id);
            registerWorker_promise.then(function (worker) {

                $ionicLoading.hide();
                $scope.register_worker_modal.hide();
                $scope.user.worker = true;
                User.setKey('worker', true);


            }, function (reason) {
                Error.show(reason);
            });
        };

        // upload on file select or drop
        $scope.upload = function (file) {
            if (file) {
                var image;
                $ionicLoading.show({
                    template: 'Uploading image...'
                });
                var reader = new FileReader();
                reader.onload = function (e) {
                    $ionicLoading.hide();
                    $scope.user.picture = e.target.result;
                    SaveImage();
                };

                var url = reader.readAsDataURL(file);
            }

        };

        var SaveImage = function () {

            AuthService.updateUser({picture: $scope.user.picture}, $scope.me.id).then(function success(resp) {
                console.log(resp);
                console.log('Success ' + resp.config.data.picture.name + 'uploaded. Response: ' + resp.data);
            }, function error(reason) {
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
                $scope.progressPercentage = 100;
            }, function notify(evt) {
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.uploadProgress = {'width': $scope.progressPercentage + '%'};
                console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.picture.name);
            });

        };

    })
    .controller('CommentsCtrl', function ($scope, $state, $ionicPopup, FeedService) {
        var commentsPopup = {};

        $scope.showComments = function (post) {
            FeedService.getPostComments(post)
                .then(function (data) {
                    post.comments_list = data;
                    commentsPopup = $ionicPopup.show({
                        cssClass: 'popup-outer comments-view',
                        templateUrl: 'views/app/partials/comments.html',
                        scope: angular.extend($scope, {current_post: post}),
                        title: post.comments + ' Comments',
                        buttons: [
                            {text: '', type: 'close-popup ion-ios-close-outline'}
                        ]
                    });
                });
        };

        //CLICK IN USER NAME
        $scope.navigateToUserProfile = function (user) {
            commentsPopup.close();
            $state.go('app.profile.posts', {userId: user.id});
        };
    })

    .controller('NewPostCtrl', function ($scope, $ionicModal, $ionicLoading, $timeout, $cordovaImagePicker, $ionicPlatform, GooglePlacesService) {
        $scope.status_post = {
            audience: 'public',
            text: '',
            images: [],
            location: ''
        };

        $ionicModal.fromTemplateUrl('views/app/partials/new_status_post.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.new_status_post_modal = modal;
        });

        $ionicModal.fromTemplateUrl('views/app/partials/checkin_status_post.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.checkin_status_post_modal = modal;
        });

        $scope.newStatusPost = function () {
            $scope.new_status_post_modal.show();
        };

        $scope.newImageStatusPost = function () {
            $scope.new_status_post_modal.show();
            $scope.openImagePicker();
        };

        $scope.openImagePicker = function () {
            //We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
            //implemented for iOS and Android 4.0 and above.

            $ionicPlatform.ready(function () {
                $cordovaImagePicker.getPictures()
                    .then(function (results) {
                        for (var i = 0; i < results.length; i++) {
                            console.log('Image URI: ' + results[i]);
                            $scope.status_post.images.push(results[i]);
                        }
                    }, function (error) {
                        // error getting photos
                    });
            });
        };

        $scope.removeImage = function (image) {
            $scope.status_post.images = _.without($scope.status_post.images, image);
        };

        $scope.closeStatusPost = function () {
            $scope.new_status_post_modal.hide();
        };

        $scope.closeCheckInModal = function () {
            $scope.predictions = [];
            $scope.checkin_status_post_modal.hide();
        };

        $scope.checkinStatusPost = function () {
            $scope.new_status_post_modal.hide();
            $scope.checkin_status_post_modal.show();
            $scope.search = {input: ''};
        };

        $scope.getPlacePredictions = function (query) {
            if (query !== "") {
                GooglePlacesService.getPlacePredictions(query)
                    .then(function (predictions) {
                        $scope.predictions = predictions;
                    });
            } else {
                $scope.predictions = [];
            }
        };

        $scope.selectSearchResult = function (result) {
            $scope.search.input = result.description;
            $scope.predictions = [];
            $scope.closeCheckInModal();

            $scope.new_status_post_modal.show();
            $scope.status_post.location = result.terms[0].value;

        };

        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.new_status_post_modal.remove();
        });

        $scope.postStatus = function () {
            $ionicLoading.show({
                template: 'Posting ...'
            });
            console.log('Posting status', $scope.status_post);

            // Simulate a posting delay. Remove this and replace with your posting code
            $timeout(function () {
                $ionicLoading.hide();
                $scope.closeStatusPost();
            }, 1000);
        };
    })

    .controller('CategoryFeedCtrl', function ($scope, _, FeedService, $stateParams, loggedUser, feed, category) {
        $scope.loggedUser = loggedUser;
        $scope.cards = feed.posts;
        $scope.current_category = category;

        $scope.page = 1;// Default page is 1
        $scope.totalPages = feed.totalPages;

        // Check if we are loading posts from one category or trend
        var categoryId = $stateParams.categoryId;

        $scope.is_category_feed = true;

        $scope.getNewData = function () {
            // Do something to load your new data here
            $scope.$broadcast('scroll.refreshComplete');
        };

        $scope.loadMoreData = function () {
            $scope.page += 1;

            console.log("Get categories feed");
            // get category feed
            FeedService.getFeedByCategory($scope.page, categoryId)
                .then(function (data) {
                    //We will update this value in every request because new posts can be created
                    $scope.totalPages = data.totalPages;
                    $scope.cards = $scope.cards.concat(data.posts);

                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
        };

        $scope.moreDataCanBeLoaded = function () {
            return $scope.totalPages > $scope.page;
        };
    })

    .controller('TrendFeedCtrl', function ($scope, _, FeedService, $stateParams, loggedUser, feed, trend) {
        $scope.loggedUser = loggedUser;
        $scope.cards = feed.posts;
        $scope.current_trend = trend;

        $scope.page = 1;// Default page is 1
        $scope.totalPages = feed.totalPages;

        // Check if we are loading posts from one category or trend
        var trendId = $stateParams.trendId;

        $scope.is_trend_feed = true;

        $scope.getNewData = function () {
            // Do something to load your new data here
            $scope.$broadcast('scroll.refreshComplete');
        };

        $scope.loadMoreData = function () {
            $scope.page += 1;

            console.log("Get trends feed");
            // get trend feed
            FeedService.getFeedByTrend($scope.page, trendId)
                .then(function (data) {
                    //We will update this value in every request because new posts can be created
                    $scope.totalPages = data.totalPages;
                    $scope.cards = $scope.cards.concat(data.posts);

                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
        };

        $scope.moreDataCanBeLoaded = function () {
            return $scope.totalPages > $scope.page;
        };
    })

    .controller('BrowseCtrl', function ($scope, trends, categories) {
        $scope.trends = trends;
        $scope.categories = categories;
    })

    .controller('EmailComposerCtrl', function ($scope, $cordovaEmailComposer, $ionicPlatform) {
        //we use email composer cordova plugin, see the documentation for mor options: http://ngcordova.com/docs/plugins/emailComposer/
        $scope.sendMail = function () {
            $ionicPlatform.ready(function () {
                $cordovaEmailComposer.isAvailable().then(function () {
                    // is available
                    console.log("Is available");
                    $cordovaEmailComposer.open({
                        to: 'hi@startapplabs.com',
                        subject: 'Nice Theme!',
                        body: 'How are you? Nice greetings from Social App'
                    }).then(null, function () {
                        // user cancelled email
                    });
                }, function () {
                    // not available
                    console.log("Not available");
                });
            });
        };
    })

    .controller('AppRateCtrl', function ($scope) {
        $scope.appRate = function () {
            if (ionic.Platform.isIOS()) {
                //you need to set your own ios app id
                AppRate.preferences.storeAppURL.ios = '1234555553>';
                AppRate.promptForRating(true);
            } else if (ionic.Platform.isAndroid()) {
                //you need to set your own android app id
                AppRate.preferences.storeAppURL.android = 'market://details?id=ionTheme3';
                AppRate.promptForRating(true);
            } else {
                //web only
            }
        };
    })

;
