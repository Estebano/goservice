/**
 * Created by stephane on 12/04/17.
 */
angular.module('goService.app.MessagesController', [])

    .controller('MessagesCtrl', function ($scope, $rootScope, User, MessageService, $state, $ionicLoading, $ionicPopup, NotificationsService, Error) {
        $scope.me = User.get();
        $scope.text = "";
        $scope.recipientId = $state.params.recipientId;
        $scope.recipient_first = $state.params.recipient_first;
        $scope.recipient_last = $state.params.recipient_last;
        $scope.recipient_picture = $state.params.recipient_picture;
        $scope.messages = [];
        $scope.page = 0;
        $scope.isInit = true;

        $scope.title = ($scope.recipient_first == null && $scope.recipient_last == null) ?
            'Messages' : $scope.recipient_first + ' ' + $scope.recipient_last;

        $scope.getNewData = function () {
            $scope.messages = [];
            $scope.page = 0;

            loadMessages(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.loadMoreData = function () {
            $scope.page += 1;
            console.log("loading more");
            loadMessages(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.sendMessage = function () {

            if ($scope.text != "") {

                if ($scope.isInit)
                    setNewConversation();
                else
                    envoyerMessage();

            }
        };

        var validateMessage = function (message) {
            var email_regX = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/gi;
            var phone_regX = /(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})/gi;
            message = message.replace(email_regX, " *** ");
            message = message.replace(phone_regX, " *** ");
            return message;
        };

        var envoyerMessage = function () {

            $ionicLoading.show({
                template: 'Sending message...'
            });

            var req = {
                sender_id: $scope.me.id,
                recipient_id: $scope.recipientId,
                message: validateMessage($scope.text),
                is_new: true
            };

            MessageService.sendMessage(req).then(function (messages) {
                $ionicLoading.hide();
                $scope.text = "";
                $scope.messages.unshift(messages.data.data[0]);

                NotificationsService.sendNotification($scope.me.id, $scope.recipientId, 'new_messages');


            }, function (reason) {
                Error.show(reason);
            });
        };

        //user_id and recipient_id in param
        var setNewConversation = function () {

            $scope.isInit = false;
            $ionicLoading.show({
                template: 'Setting conversation...'
            });

            MessageService.getConversation($scope.me.id, $scope.recipientId).then(function (conversation) {

                $ionicLoading.hide();
                if (conversation.data.data.length > 0)
                    envoyerMessage();
                else
                    createConversation();

            }, function (reason) {
                Error.show(reason);
            });
        };

        var createConversation = function () {
            $ionicLoading.show({
                template: 'Setting conversation...'
            });
            var req = {
                "user_id": $scope.me.id,
                "recipient_id": $scope.recipientId,
                "recipient_name_first": $scope.recipient_first,
                "recipient_name_last": $scope.recipient_last,
                "recipient_picture": $scope.recipient_picture
            };
            MessageService.createConversation(req).then(function (messages) {

                $ionicLoading.hide();
                envoyerMessage();

            }, function (reason) {
                Error.show(reason);
            });
        };

        $scope.moreDataCanBeLoaded = function () {
            return $scope.totalPages > $scope.page;
        };


        var loadMessages = function (callback) {
            $ionicLoading.show({
                template: 'Getting messages...'
            });

            MessageService.getMessages($scope.page, $scope.recipientId, $scope.me.id).then(function (messages) {

                $ionicLoading.hide();
                $scope.totalPages = Math.round(messages.data.meta.total / 10);
                $scope.messages = $scope.messages.concat(messages.data.data);
                $rootScope.notificationHasChanged = false;

                if (typeof callback !== 'undefined') callback();

            }, function (reason) {
                Error.show(reason);
            });
        };

        var unSetNotifications = function () {
            NotificationsService.getNotifications($scope.me.id, $scope.recipientId).then(
                function (result) {
                    if (result.data.data.length == 1) updateNotification(result.data.data[0].id);
                },
                function (reason) {
                    Error.show(reason);
                }
            );
        };

        var updateNotification = function (notificationId) {
            NotificationsService.updateNotifications({new_messages: 0}, notificationId)
                .then(function (result) {
                    $rootScope.notificationHasChanged = true;
                },
                function (reason) {
                    Error.show(reason);
                }
            );
        };

        loadMessages(unSetNotifications);
    })

    .controller('ConversationsCtrl', function ($scope, User, $ionicLoading, $ionicPopup, MessageService, Error, NotificationsService) {
        $scope.me = User.get();
        $scope.conversations = [];
        $scope.page = 0;

        $scope.getNewData = function () {
            $scope.conversations = [];
            $scope.page = 0;

            loadConversations(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.loadMoreData = function () {
            $scope.page += 1;
            console.log("loading more");
            loadConversations(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.moreDataCanBeLoaded = function () {
            return $scope.totalPages > $scope.page;
        };

        var loadConversations = function (callback) {
            $ionicLoading.show({
                template: 'Getting conversations...'
            });

            MessageService.getConversations($scope.page, $scope.me.id).then(function (conversations) {

                $scope.totalPages = Math.round(conversations.data.meta.total / 10);
                getNewNotifications(conversations.data.data);

                if (typeof callback !== 'undefined') callback();

            }, function (reason) {
                Error.show(reason);
            });
        };

        var getNewNotifications = function (conversations) {
            NotificationsService.getNotifications($scope.me.id).then(
                function (result) {
                    $ionicLoading.hide();
                    setNewMessages(conversations, result.data.data);
                },
                function (reason) {
                    Error.show(reason);
                }
            );
        };

        var setNewMessages = function (conversations, notifications) {

            conversations.map(function (conversation) {

                var hasNewMessages = _.find(notifications, function (notification) {
                    return notification.user_id == conversation.recipient_id;
                });

                if (hasNewMessages) conversation.new_messages = hasNewMessages.new_messages;
            });

            $scope.conversations = $scope.conversations.concat(conversations);
        };

        loadConversations();


    });
