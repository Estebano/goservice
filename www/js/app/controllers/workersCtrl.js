angular.module('goService.app.workersController', [])

    .controller('WorkersCtrl', function ($scope, User, WorkerService, $ionicLoading, $ionicPopup, CategoryList, $state, $geolocation, Distance, Error) {
        $scope.me = User.get();
        $scope.workers = [];
        $scope.page = 0;

        $scope.req_params = {
            category: null,
            position: null,
            rating: null
        };
        $scope.my_location = null;
        $scope.categories = CategoryList.get();
        $scope.my_state = null;

        $scope.getNewData = function () {
            $scope.workers = [];
            $scope.page = 0;

            loadData(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.filterWorkers = function () {
            $scope.workers = [];
            $scope.page = 0;

            loadData(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.loadMoreData = function () {
            $scope.page += 1;
            console.log("loading more");
            loadData(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.moreDataCanBeLoaded = function () {
            return $scope.totalPages > $scope.page;
        };

        var loadData = function (callback) {
            $ionicLoading.show({
                template: 'Getting workers...'
            });

            var getWorkers_promise = WorkerService.getWorkers($scope.page, $scope.req_params, $scope.me.state);
            getWorkers_promise.then(function (workers) {
                $ionicLoading.hide();
                $scope.totalPages = Math.round(workers.data.meta.total / 10);
                var sorted_workers = getDistance(workers.data.data);
                $scope.workers = $scope.workers.concat(sorted_workers);

                callback();

            }, function (reason) {
                Error.show(reason);
            });
        };

        var getDistance = function (workers) {
            workers.map(function (worker) {
                var distance = $scope.my_location == null ? 'Not Available' : Distance.getDistanceFromLatLonInKm($scope.my_location.latitude, $scope.my_location.longitude, worker.lat, worker.lng);
                worker.distance = distance.toFixed(2) + ' Km';
            });
            return workers;
        };

        $ionicLoading.show({
            template: 'loading...'
        });

        $geolocation.getCurrentPosition({
            timeout: 60000
        }).then(function (position) {
            $ionicLoading.hide();
            console.log(position);
            $scope.my_location = position.coords;

            loadData(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }, function (reason) {
            Error.show(reason);
            loadData(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        });

    });
