/**
 * Created by stephane on 5/05/17.
 */
/**
 * Created by stephane on 20/03/17.
 */
angular.module('goService.app.bookAppointmentController', [])

    .controller('BookAppointmentCtrl', function ($scope, User, BookingService, Error, $ionicLoading, NotificationsService) {
        $scope.me = User.get();

        $scope.req = {
            user_id: $scope.me.id,
            recipient_id: $scope.user.id,
            address: "",
            title: "",
            date: null,
            detail: "",
            completed: false
        };

        $scope.date = null;
        $scope.time = null;

        $scope.book = function () {

            $ionicLoading.show({
                template: 'Booking...'
            });

            $scope.req.date = formatDate($scope.date, $scope.time);
            getBooking();

        };

        var resetForm = function () {
            $scope.req = {
                user_id: $scope.me.id,
                recipient_id: $scope.user.id,
                address: "",
                title: "",
                date: null,
                detail: "",
                completed: false
            };

            $scope.date = null;
            $scope.time = null;
        };

        var postBooking = function () {
            BookingService.setBooking($scope.req).then(
                function (result) {
                    $ionicLoading.hide();
                    $scope.appointment_modal.hide();

                    NotificationsService.sendNotification($scope.me.id, $scope.req.recipient_id, 'new_bookings');

                    resetForm();

                },
                function (reason) {
                    Error.show(reason);
                }
            );
        };

        var formatDate = function (date, time) {
            var date = moment(date).format('DD/MM/YYYY') + " " + moment(time).format('HH:mm:ss');
            date = moment(date, 'DD/MM/YYYY HH:mm:ss', true).format('YYYY-MM-DDTHH:mm:ss') + '.000Z';

            return date;
        };

        var getBooking = function () {
            BookingService.getBooking(
                $scope.req.recipient_id,
                $scope.req.date,
                $scope.req.user_id).then(
                function (result) {
                    if (result.data.data.length == 0) {
                        postBooking();
                    } else {
                        $ionicLoading.hide();
                        Error.show("Time frame not available. Please select another date");
                    }
                },
                function (reason) {
                    Error.show(reason);
                }
            );
        };


    });


