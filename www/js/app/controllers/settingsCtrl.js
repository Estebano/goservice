angular.module('goService.app.settingsControllers', [])

    .controller('SettingsCtrl', function ($scope, $ionicModal) {
        $ionicModal.fromTemplateUrl('views/app/legal/terms-of-service.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.terms_of_service_modal = modal;
        });

        $ionicModal.fromTemplateUrl('views/app/legal/privacy-policy.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.privacy_policy_modal = modal;
        });

        $scope.showTerms = function () {
            $scope.terms_of_service_modal.show();
        };

        $scope.showPrivacyPolicy = function () {
            $scope.privacy_policy_modal.show();
        };
    })

    .controller('EditProfileCtrl', function ($scope, $ionicModal, AuthService, WorkerService, User, CategoryList, $state, $ionicLoading) {
        $scope.user = User.get();
        $scope.categories = CategoryList.get();
        $scope.isUndefinedWorker = typeof $scope.user.worker_info === 'undefined';
        var postal_code = User.getByKey('postal_code');

        if ($scope.user.worker && $scope.isUndefinedWorker) $state.go('app.profile.post', {userId: $scope.user.id}, {reload: true});

        var worker_id = $scope.user.worker_info.id;
        var worker_req = {};


        $scope.doEdit = function () {

            if ((postal_code != $scope.user.postal_code) && $scope.user.worker) {
                setLatLng(updateUserInfo);
            } else {
                updateUserInfo(updateWorkerInfo);
            }

        };

        var setLatLng = function (callback) {

            $ionicLoading.show({
                template: 'Getting coordinates...'
            });

            var location = $scope.user.postal_code + ' ' + $scope.user.state;

            WorkerService.getLatLng(location).then(function success(latLng) {
                $ionicLoading.hide();
                worker_req.lat = latLng.data.results[0].geometry.location.lat;
                worker_req.lng = latLng.data.results[0].geometry.location.lng;

                callback(updateWorkerInfo);
            }, function error(reason) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
            });

        };

        var updateUserInfo = function (callback) {

            var user_req = {
                name_first: $scope.user.name_first,
                name_last: $scope.user.name_last,
                postal_code: $scope.user.postal_code,
                state: $scope.user.state
            };

            $ionicLoading.show({
                template: 'Updating user information...'
            });

            AuthService.updateUser(user_req, $scope.user.id).then(function success(response) {
                $ionicLoading.hide();
                User.set(response.data.data[0]);

                if ($scope.user.worker) callback();
                else $state.go('app.settings');

            }, function error(reason) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
            });
        };

        var updateWorkerInfo = function () {

            worker_req.category = $scope.user.worker_info.category;
            worker_req.about = $scope.user.worker_info.about;
            worker_req.hourly_rate = $scope.user.worker_info.hourly_rate;
            worker_req.cover = $scope.user.worker_info.cove;


            $ionicLoading.show({
                template: 'Updating worker information...'
            });

            AuthService.updateWorker(worker_req, worker_id).then(function success(response) {
                $ionicLoading.hide();
                User.setKey('worker_info', response.data.data[0]);
                $state.go('app.settings');
                console.log(User.get());
            }, function error(reason) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
            });
        };

    });


