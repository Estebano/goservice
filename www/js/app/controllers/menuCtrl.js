/**
 * Created by stephane on 19/03/17.
 */
angular.module('goService.app.menuController', [])

    .controller('MenuCtrl', function ($scope, $rootScope, User, Error, NotificationsService) {

        $scope.getUserId = function () {
            return JSON.stringify(User.getByKey('id'));
        };

        var user = User.get();
        $scope.newMessages = 0;
        $scope.newBookings = 0;
        $rootScope.notificationHasChanged = false;
        var isInit = true;

        var init = function () {
            NotificationsService.getNotifications(user.id).then(
                function (result) {
                    $scope.newMessages = sumNewNotification(result.data.data, 'new_messages');
                    $scope.newBookings = sumNewNotification(result.data.data, 'new_bookings');
                },
                function (reason) {
                    Error.show(reason);
                }
            );
        };

        var sumNewNotification = function (data, type) {
            var typeArray = _.map(data, function (val) {
                return val[type]
            });
            return _.reduce(typeArray, function (x, y) {
                return x + y;
            });
        };

        $scope.$watch(
            "notificationHasChanged",
            function (newValue, oldValue) {
                if (!isInit) init();
                isInit = false;
            }
        );

        init();

    })

    .controller('AppCtrl', function ($scope, User, Error, NotificationsService) {


    });
