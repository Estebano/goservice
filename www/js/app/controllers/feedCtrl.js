angular.module('goService.app.feedsController', [])

    .controller('FeedCtrl', function ($scope, FeedService, CategoryList, $stateParams, User, $ionicLoading, $ionicPopup) {
        $scope.cards = [];
        $scope.loggedUser = User.get();
        $scope.page = 0;
        $scope.category = null;
        $scope.my_location = null;
        $scope.categories = CategoryList.get();

        $scope.getNewData = function () {
            $scope.cards = [];
            $scope.page = 0;

            loadData(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.filterAds = function (category, position) {
            $scope.cards = [];
            $scope.page = 0;
            $scope.category = category;
            $scope.position = typeof position === 'undefined' ? $scope.position : position;

            loadData();
        };

        $scope.loadMoreData = function () {
            $scope.page += 1;
            console.log("loading more");
            loadData(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.moreDataCanBeLoaded = function () {
            return $scope.totalPages > $scope.page;
        };

        var loadData = function (callback) {
            $ionicLoading.show({
                template: 'Getting posts...'
            });
            FeedService.getFeed($scope.page, $scope.category, $scope.loggedUser.state, $scope.position)
                .then(function (cards) {
                    $ionicLoading.hide();
                    $scope.totalPages = Math.round(cards.data.meta.total / 10);
                    $scope.cards = $scope.cards.concat(cards.data.data);

                    if (typeof callback !== "undefined") callback();

                }, function (reason) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'ERROR',
                        template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                    });
                });
        };

        loadData();
    })
    .controller('CreateAdCtrl', function ($scope, User, CategoryList, FeedService, $ionicLoading, $ionicPopup, $state) {
        $scope.me = User.get();
        $scope.ad = {
            title: '',
            description: '',
            category: '',
            user_id: $scope.me.id,
            picture: null
        };

        $scope.progressPercentage = 0;
        $scope.uploadProgress = {'width': '0%'};

        $scope.categories = CategoryList.get();

        $scope.openImagePicker = function (file) {
            if (file) {

                $ionicLoading.show({
                    template: 'Uploading image...'
                });

                var reader = new FileReader();

                reader.onload = function (e) {
                    $ionicLoading.hide();
                    $scope.ad.picture = e.target.result;
                };

                var url = reader.readAsDataURL(file);
            }
        };

        $scope.submitAd = function () {
            $ionicLoading.show({
                template: 'Creating ad...'
            });
            FeedService.createAd($scope.ad).then(
                function (result) {
                    $ionicLoading.hide();
                    $state.go('app.profile.posts', {userId: $scope.me.id}, {reload: true});
                },
                function (reason) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'ERROR',
                        template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                    });
                },
                function (evt) {
                    $ionicLoading.hide();
                    $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.uploadProgress = {'width': $scope.progressPercentage + '%'};
                });

        };
    })
    .controller('PostDetailsCtrl', function ($scope, FeedService, $ionicPopup, $ionicLoading, $state, User, $ionicModal) {
        $scope.me = User.get();
        $scope.myProfile = false;
        var postId = $state.params.postId;

        $ionicLoading.show({
            template: 'Getting post...'
        });

        $ionicModal.fromTemplateUrl('views/app/partials/view_image.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.view_image_modal = modal;
        });

        FeedService.getPost(postId).then(function (post) {
            $ionicLoading.hide();
            $scope.post = post.data.data[0];
            $scope.myProfile = $scope.me.id == $scope.post.user.id;
        }, function (reason) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'ERROR',
                template: reason.data != null ? reason.data.meta.error.message : reason.statusText
            });
        });

        $scope.viewImage = function () {
            $scope.view_image_modal.show();
        };

        $scope.closeImage = function () {
            $scope.view_image_modal.hide();
        };

        $scope.deleteAd = function (postId) {
            $ionicLoading.show({
                template: 'Deleting post...'
            });
            FeedService.deletePost(postId).then(function (post) {
                $ionicLoading.hide();
                $state.go('app.feed', {reload: true});
            }, function (reason) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
            });
        };
    });