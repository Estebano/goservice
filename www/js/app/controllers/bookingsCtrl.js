/**
 * Created by stephane on 20/03/17.
 */
angular.module('goService.app.bookingsController', [])

    .controller('BookingsCtrl', function ($scope, User) {
        $scope.me = User.get();
    })

    .controller('ClientsCtrl', function ($scope, User) {
        $scope.me = User.get();
    })

    .controller('ServicesCtrl', function ($scope, User) {
        $scope.me = User.get();
    })

    .controller('TransactionsCtrl', function ($scope, User) {
        $scope.me = User.get();
    });