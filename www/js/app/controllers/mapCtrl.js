/**
 * Created by stephane on 20/03/17.
 */
angular.module('goService.app.mapController', [])

    .controller('MapCtrl', function ($scope, User, NgMap, $geolocation, $ionicLoading, $ionicPopup, WorkerService, $stateParams, CategoryList, Error) {

        $scope.$on("$ionicView.enter", function (event, data) {
            init();
            console.log("Reloading....");
        });

        $scope.me = User.get();
        $scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_l2XHC8Wn7zY6xMgfqRPjbMqDz2PuUgs";
        $scope.map = {};
        $scope.my_location = null;
        $scope.workers = [];
        $scope.isSearchOpen = false;
        $scope.isSingleView = $stateParams.workerId != null;
        $scope.categories = CategoryList.get();

        $scope.worker_params = $stateParams.workerId == null ?
        {
            category: null,
            position: null,
            rating: null
        }
            : $stateParams.workerId;

        var getWorkers_promise = $stateParams.workerId == null ? WorkerService.getWorkersByLocation : WorkerService.getWorker;


        var getCurrentLocation = function() {
            $ionicLoading.show({template: 'Getting your position...'});

            $geolocation.getCurrentPosition({
                timeout: 60000
            }).then(function(position) {
                $ionicLoading.hide();
                $scope.my_location = position.coords;
                $scope.map.setCenter({lat: position.coords.latitude, lng: position.coords.longitude});

                getWorkers();

            }, function (reason) {
                Error.show(reason);
            });
        };

        $scope.clickMarker = function (ev) {
        };

        $scope.filterWorkers = function () {
            $scope.workers = [];
            getWorkers();
        };

        var setWorkersMarkers = function () {
            var bounds = new google.maps.LatLngBounds();
            var myLatLng = new google.maps.LatLng($scope.my_location.latitude, $scope.my_location.longitude);
            bounds.extend(myLatLng);

            $scope.workers.map(function (worker) {
                var newLatLng = new google.maps.LatLng(parseFloat(worker.lat), parseFloat(worker.lng));
                bounds.extend(newLatLng);
            });

            $scope.map.setCenter(bounds.getCenter());
            $scope.map.fitBounds(bounds);
        };

        var getWorkers = function () {
            $ionicLoading.show({
                template: 'Getting workers...'
            });

            getWorkers_promise($scope.worker_params).then(function (workers) {
                $ionicLoading.hide();
                $scope.workers = $scope.workers.concat(workers.data.data);

                setWorkersMarkers();

            }, function (reason) {
                Error.show(reason);
            });
        };

        var init = function () {
            $ionicLoading.show({template: 'Loading map...'});
            NgMap.getMap().then(function (map) {
                $ionicLoading.hide();

                $scope.map = map;
                google.maps.event.trigger($scope.map, "resize");

                getCurrentLocation();

            });
        };


    });
