/**
 * Created by stephane on 20/03/17.
 */
angular.module('goService.app.notificationsController', [])

    .controller('NotificationsCtrl', function ($scope, User) {
        $scope.me = User.get();
    })
;
