angular.module('goService.app.services', [])

    .service('AuthService', function ($http, User, APP_CONSTANT) {

        this.getAuthUserId = function (authUser) {
            var username = authUser.username,
                password = authUser.password;

            var req = {
                method: 'POST',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/access_tokens',
                data: {
                    "grant_type": "password",
                    "username": username,
                    "password": password
                },
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.getUser = function (userId) {

            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/users?id=' + userId,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.getWorker = function (userId) {

            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/workers?user_id=' + userId,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.registerUser = function (userData) {

            var req = {
                method: 'POST',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/users',
                data: userData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.registerWorker = function (userData) {

            var req = {
                method: 'POST',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/workers',
                data: userData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.updateUser = function (userData, userId) {

            var req = {
                method: 'PUT',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/users/' + userId,
                data: userData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.updateWorker = function (workerData, workerId) {

            var req = {
                method: 'PUT',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/workers/' + workerId,
                data: workerData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

    })
    .service('NotificationsService', function ($http, User, APP_CONSTANT, $ionicLoading, MessageService, Error) {
        var _this = this;

        this.getNotifications = function (recipient_id, userId) {
            var user_id = (typeof userId !== 'undefined') ? '&user_id=' + userId : '';

            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/notifications?recipient_id=' + recipient_id + user_id,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.updateNotifications = function (notificationData, notificationId) {

            var req = {
                method: 'PUT',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/notifications/' + notificationId,
                data: notificationData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.setNotifications = function (notificationData) {

            var req = {
                method: 'POST',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/notifications',
                data: notificationData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.sendNotification = function (userId, recipientId, type) {
            $ionicLoading.show({
                template: 'Sending notification...'
            });

            _this.getNotifications(recipientId, userId).then(
                function (result) {

                    if (result.data.data.length == 0) {
                        _this.createNotification(userId, recipientId, type);
                    } else {
                        var req = {},
                            newNotif = (result.data.data[0][type] == null) ? 1 : result.data.data[0][type] + 1;

                        req[type] = newNotif;
                        _this.incrementNotification(req, result.data.data[0].id);
                    }

                },
                function (reason) {
                    Error.show(reason);
                }
            );

        };

        this.createNotification = function (userId, recipientId, type) {
            var notificationData = {
                "user_id": userId,
                "recipient_id": recipientId
            };
            notificationData[type] = 1;

            _this.setNotifications(notificationData).then(
                function (result) {
                    _this.setRecipientProcess(recipientId, type);
                },
                function (reason) {
                    Error.show(reason);
                });
        };

        this.incrementNotification = function (data, notificationId) {

            _this.updateNotifications(data, notificationId).then(
                function (result) {
                    $ionicLoading.hide();
                },
                function (reason) {
                    Error.show(reason);
                });

        };

        this.setRecipientProcess = function (recipientId, type) {
            var user = User.get();

            _this.processType[type](recipientId, user);
        };

        this.processType = {
            'new_messages': function (recipientId, user) {
                var conversationData = {
                    "user_id": recipientId,
                    "recipient_id": user.id,
                    "recipient_name_first": user.name_first,
                    "recipient_name_last": user.name_last,
                    "recipient_picture": user.picture
                };
                MessageService.getConversation(recipientId, user.id).then(
                    function (result) {

                        if (result.data.data.length == 0) {

                            MessageService.createConversation(conversationData).then(
                                function (result) {
                                    $ionicLoading.hide();
                                },
                                function (reason) {
                                    Error.show(reason);
                                }
                            );

                        } else {
                            $ionicLoading.hide();
                        }
                    },
                    function (reason) {
                        Error.show(reason);
                    });
            },
            'new_bookings': function (recipientId, user) {
            }
        };
    })
    .service('WorkerService', function ($http, $q, APP_CONSTANT) {

        this.getWorkers = function (page, req_params, state) {

            var category_req = (req_params.category != null && req_params.category != '') ? '&category__istartswith=' + req_params.category : '';
            var position_req = (req_params.position != null && req_params.position != '') ? '&about__icontains=' + req_params.position : '';
            var state = state == null ? '' : '&user__state=' + state;
            var rating_req = (req_params.rating != null && req_params.rating != '') ? '&user__rate__gte=' + req_params.rating : '';

            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/workers?visible=true&__count=10&__offset=' + (page * 10) + state + category_req + position_req + rating_req,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.getWorkersByLocation = function (params) {

            var category_req = (params.category != null && params.category != '') ? '&category__istartswith=' + params.category : '';
            var position_req = (params.position != null && params.position != '') ? '&about__icontains=' + params.position : '';
            var rating_req = (params.rating != null && params.rating != '') ? '&user__rate__gte=' + params.rating : '';
            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/workers?visible=true' + category_req + position_req + rating_req,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.getWorker = function (worker_id) {

            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/workers?id=' + worker_id,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.getLatLng = function (location) {

            var req = {
                method: 'GET',
                url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + location,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.getAddress = function (latLng) {
            //seperate by lat,lng
            var key = '&key=AIzaSyA_l2XHC8Wn7zY6xMgfqRPjbMqDz2PuUgs';
            var req = {
                method: 'GET',
                url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latLng + key,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        }

    })
    .service('BookingService', function ($http, APP_CONSTANT) {

        this.setBooking = function (bookingData) {

            var req = {
                method: 'POST',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/bookings',
                data: bookingData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.updateBooking = function (bookingData, bookingId) {

            var req = {
                method: 'PUT',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/bookings/' + bookingId,
                data: bookingData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

        this.getBooking = function (recipientId, date, userId) {
            var user_id = (typeof userId === 'undefined') ? "" : '&user_id=' + userId;

            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/bookings?completed=false&recipient_id=' + recipientId + '&date=' + date + user_id,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);

        };

    })
    .service('ProfileService', function ($http, $q) {

        this.getUserPictures = function (userId) {
            var dfd = $q.defer();

            $http.get('database.json').success(function (database) {
                //get user related pictures
                var user_pictures = _.filter(database.users_pictures, function (picture) {
                    return picture.userId == userId;
                });

                dfd.resolve(user_pictures);
            });

            return dfd.promise;
        };

        this.getUserPosts = function (userId) {
            var dfd = $q.defer();

            $http.get('database.json').success(function (database) {
                //get user related pictures
                var user_post = _.filter(database.posts, function (post) {
                    return post.userId == userId;
                });

                dfd.resolve(user_post);
            });

            return dfd.promise;
        };


    })
    .service('MessageService', function ($http, APP_CONSTANT) {

        this.sendMessage = function (userData) {
            var req = {
                method: 'POST',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/messages',
                data: userData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.getConversation = function (user_id, recipient_id) {
            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/conversations?user_id=' + user_id + '&recipient_id=' + recipient_id,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.getConversations = function (page, user_id) {
            var userId = '&user_id=' + user_id;
            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/conversations?__count=10&__offset=' + (page * 10) + userId,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };

            return $http(req);
        };

        this.createConversation = function (userData) {
            var req = {
                method: 'POST',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/conversations',
                data: userData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            return $http(req);
        };

        this.getMessages = function (page, recipientId, userId) {

            var sender_id = '&from=' + userId;
            var recipient_id = '&to=' + recipientId;

            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/messages?__count=10&__offset=' + (page * 10) + sender_id + recipient_id,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            return $http(req);
        };

    })

    .service('FeedService', function ($http, $q, APP_CONSTANT) {

        this.createAd = function (userData) {
            var req = {
                method: 'POST',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/ads',
                data: userData,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            return $http(req);
        };

        this.getMyPosts = function (userId) {
            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/ads?user_id=' + userId,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            return $http(req);
        };

        this.getPost = function (postId) {
            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/ads?id=' + postId,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            return $http(req);
        };

        this.deletePost = function (postId) {
            var req = {
                method: 'DELETE',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/ads/' + postId,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            return $http(req);
        };

        this.getFeed = function (page, category, state, title) {
            var category_req = (category != null && category != '') ? '&category__istartswith=' + category : '';
            var title_req = (title != null && position != '') ? '&title__icontains=' + title : '';
            var state = state == null ? '' : '&user__state=' + state;
            var req = {
                method: 'GET',
                url: APP_CONSTANT.api.url + '/' + APP_CONSTANT.api.version + '/ads?__count=10&__offset=' + (page * 10) + state + category_req + title_req,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            return $http(req);
        };

        this.getPostComments = function (post) {
            var dfd = $q.defer();

            $http.get('database.json').success(function (database) {
                var comments_users = database.users;
                // Randomize comments users array
                comments_users = window.knuthShuffle(comments_users.slice(0, post.comments));

                var comments_list = [];
                // Append comment text to comments list
                comments_list = _.map(comments_users, function (user) {
                    var comment = {
                        user: user,
                        text: database.comments[Math.floor(Math.random() * database.comments.length)].comment
                    };
                    return comment;
                });

                dfd.resolve(comments_list);
            });

            return dfd.promise;
        };

    })

    .service('PeopleService', function ($http, $q) {

        this.getPeopleSuggestions = function () {

            var dfd = $q.defer();

            $http.get('database.json').success(function (database) {

                var people_suggestions = _.each(database.people_suggestions, function (suggestion) {
                    suggestion.user = _.find(database.users, function (user) {
                        return user.id == suggestion.userId;
                    });

                    //get user related pictures
                    var user_pictures = _.filter(database.users_pictures, function (picture) {
                        return picture.userId == suggestion.userId;
                    });

                    suggestion.user.pictures = _.last(user_pictures, 3);

                    return suggestion;
                });

                dfd.resolve(people_suggestions);
            });

            return dfd.promise;
        };

        this.getPeopleYouMayKnow = function () {

            var dfd = $q.defer();

            $http.get('database.json').success(function (database) {

                var people_you_may_know = _.each(database.people_you_may_know, function (person) {
                    person.user = _.find(database.users, function (user) {
                        return user.id == person.userId;
                    });
                    return person;
                });

                dfd.resolve(people_you_may_know);
            });

            return dfd.promise;
        };
    })

    .service('GooglePlacesService', function ($q) {
        this.getPlacePredictions = function (query) {
            var dfd = $q.defer();
            var service = new google.maps.places.AutocompleteService();

            service.getPlacePredictions({input: query},
                function (predictions, status) {
                    if (status != google.maps.places.PlacesServiceStatus.OK) {
                        dfd.resolve([]);
                    }
                    else {
                        dfd.resolve(predictions);
                    }
                });
            return dfd.promise;
        }
    })


    .service('User', function ($window) {

        var _this = this;

        this.set = function (val) {
            $window.localStorage['user'] = JSON.stringify(val);
        };

        this.setKey = function (key, val) {
            var user = _this.get();

            user[key] = val;

            _this.set(user);
        };

        this.getByKey = function (key) {
            var user = _this.get();
            return user[key];
        };


        this.get = function () {
            return JSON.parse($window.localStorage['user']);
        }

    })

    .service('Distance', function () {
        var _this = this;

        this.getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = _this.deg2rad(lat2 - lat1);  // deg2rad below
            var dLon = _this.deg2rad(lon2 - lon1);
            var a =
                    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.cos(_this.deg2rad(lat1)) * Math.cos(_this.deg2rad(lat2)) *
                    Math.sin(dLon / 2) * Math.sin(dLon / 2)
                ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c; // Distance in km
            return d;
        };

        this.deg2rad = function (deg) {
            return deg * (Math.PI / 180)
        };
    })

    .service('CategoryList', function () {

        this.get = function () {
            return [
                'Accounting',
                'Administration',
                'Advertising, Arts & Media',
                'Banking',
                'Beauty',
                'Customer',
                'Community',
                'ChildCare',
                'Construction',
                'Design',
                'Education',
                'Gardening',
                'Farming',
                'Hospitality',
                'IT',
                'Mechanics'
            ];
        }

    })

    .service('Error', function ($ionicLoading, $ionicPopup) {

        this.show = function (reason) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'ERROR',
                template: (typeof reason === 'string') ? reason :
                    reason.data != null ? reason.data.meta.error.message : reason.statusText
            });
        }

    })

    .filter('reverse', function () {
        return function (items) {
            return items.slice().reverse();
        };
    })


;
