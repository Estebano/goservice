angular.module('underscore', [])
    .factory('_', function () {
        return window._; // assumes underscore has already been loaded on the page
    });

angular.module('goService', [
    'ionic',
    'facebook',
    'goService.common.directives',
    'goService.app.services',
    'goService.app.filters',
    'goService.app.controllers',
    'goService.auth.controllers',
    'goService.views',
    'underscore',
    'angularMoment',
    'ngCordova',
    'ngMessages',
    'ngFileUpload',
    'ngMap',
    'ngGeolocation',
    'monospaced.elastic'
])

    .constant('APP_CONSTANT', {
        api: {
            url: 'http://172.20.10.2:3000',
            version: 'v1'
        }
    })
    .run(function ($ionicPlatform, $rootScope, User, $state, $window, $ionicPopup) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $window.addEventListener("offline", function () {
            $ionicPopup.alert({
                title: 'ERROR',
                template: 'No internet connection'
            });
            User.set('');
            $state.go('facebook-sign-in');
        }, false);

    })
    .config(function ($stateProvider, $urlRouterProvider, FacebookProvider) {

        FacebookProvider.init('709252109256569');

        $stateProvider

            //SIDE MENU ROUTES
            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "views/app/side-menu.html",
                controller: 'AppCtrl'
            })

            .state('app.map', {
                url: "/map",
                params: {
                    workerId: null
                },
                views: {
                    'menuContent': {
                        templateUrl: "views/app/map/map.html",
                        controller: "MapCtrl"
                    }
                }
            })

            .state('app.feed', {
                url: "/feed",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/feed.html",
                        controller: "FeedCtrl"
                    }
                }
            })

            .state('app.conversations', {
                url: "/conversations/:userId",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/conversations.html",
                        controller: "ConversationsCtrl"
                    }
                }
            })

            .state('app.messages', {
                url: "/messages/:recipientId",
                params: {
                    recipient_first: null,
                    recipient_last: null,
                    recipient_picture: null
                },
                views: {
                    'menuContent': {
                        templateUrl: "views/app/messages.html",
                        controller: "MessagesCtrl"
                    }
                }
            })

            .state('app.post', {
                url: "/post/:postId",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/post/details.html",
                        controller: 'PostDetailsCtrl'
                    }
                }
            })

            .state('app.profile', {
                abstract: true,
                url: '/profile/:userId',
                views: {
                    'menuContent': {
                        templateUrl: "views/app/profile/profile.html",
                        controller: 'ProfileCtrl'
                    }
                },
                resolve: {
                    pictures: function (ProfileService, $stateParams) {
                        var profileUserId = 1;
                        return ProfileService.getUserPictures(profileUserId);
                    }
                }
            })

            .state('app.profile.posts', {
                url: '/posts',
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.details.html'
                    },
                    'profileSubContent@app.profile.posts': {
                        templateUrl: 'views/app/profile/profile.posts.html'
                    }
                }
            })

            .state('app.profile.reviews', {
                url: '/reviews',
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.details.html'
                    },
                    'profileSubContent@app.profile.posts': {
                        templateUrl: 'views/app/profile/profile.reviews.html'
                    }
                }
            })

            .state('app.profile.pics', {
                url: '/pics',
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.details.html'
                    },
                    'profileSubContent@app.profile.pics': {
                        templateUrl: 'views/app/profile/profile.pics.html'
                    }
                }
            })

            .state('app.workers', {
                url: "/workers",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/workers.html",
                        controller: "WorkersCtrl"
                    }
                }
            })

            .state('app.settings', {
                url: "/settings",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/profile/settings.html",
                        controller: 'SettingsCtrl'
                    }
                }
            })

            .state('app.edit_profile', {
                url: "/edit-profile",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/profile/edit-profile.html",
                        controller: 'EditProfileCtrl'
                    }
                }
            })

            .state('app.createAd', {
                url: "/create-ad",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/profile/create-ad.html",
                        controller: 'CreateAdCtrl'
                    }
                }
            })

            .state('app.bookings', {
                url: "/bookings",
                abstract: true,
                views: {
                    'menuContent': {
                        templateUrl: "views/app/bookings/bookings.html",
                        controller: 'BookingsCtrl'
                    }
                }
            })

            .state('app.bookings.clients', {
                url: "/clients",
                views: {
                    'bookingContent': {
                        templateUrl: "views/app/bookings/clients.html",
                        controller: 'ClientsCtrl'
                    }
                }
            })

            .state('app.bookings.services', {
                url: "/services",
                views: {
                    'bookingContent': {
                        templateUrl: "views/app/bookings/services.html",
                        controller: 'ServicesCtrl'
                    }
                }
            })

            .state('app.bookings.transactions', {
                url: "/transactions",
                views: {
                    'bookingContent': {
                        templateUrl: "views/app/bookings/transactions.html",
                        controller: 'TransactionsCtrl'
                    }
                }
            })

            //AUTH ROUTE
            .state('facebook-sign-in', {
                url: "/facebook-sign-in",
                templateUrl: "views/auth/facebook-sign-in.html",
                controller: 'WelcomeCtrl'
            })

            .state('create-account', {
                url: "/create-account",
                templateUrl: "views/auth/create-account.html",
                controller: 'CreateAccountCtrl'
            })

            .state('welcome-back', {
                url: "/welcome-back",
                templateUrl: "views/auth/welcome-back.html",
                controller: 'WelcomeBackCtrl'
            })
        ;


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/facebook-sign-in');
    });
