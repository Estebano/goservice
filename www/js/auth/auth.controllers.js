angular.module('goService.auth.controllers', [])


    .controller('WelcomeCtrl', function ($scope, $state, $ionicModal, Facebook, User) {
        $scope.user = {
            userName: '',
            password: ''
        };

        var me = User.get();

        if (me != '') {
            $state.go('app.profile.posts', {userId: me.id});
        }

        var loggedIn = false;

        $scope.facebookSignIn = function () {
            getLoginStatus();
            $state.go('app.feed');
        };

        var getLoginStatus = function () {
            Facebook.getLoginStatus(function (response) {

                if (response.status === 'connected') {
                    var loggedIn = true;
                    me();
                }

            });
        };

        var me = function () {
            Facebook.api('/me', function (response) {
                $scope.user = response;
                $state.go('app.feed');
            });
        };

        $ionicModal.fromTemplateUrl('views/app/legal/privacy-policy.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.privacy_policy_modal = modal;
        });

        $ionicModal.fromTemplateUrl('views/app/legal/terms-of-service.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.terms_of_service_modal = modal;
        });

        $scope.showPrivacyPolicy = function () {
            $scope.privacy_policy_modal.show();
        };

        $scope.showTerms = function () {
            $scope.terms_of_service_modal.show();
        };
    })

    .controller('CreateAccountCtrl', function ($scope, $state, AuthService, User, $ionicLoading, $ionicPopup) {
        $scope.user = {
            name_first: "",
            name_last: "",
            password: "",
            state: "",
            postal_code: "",
            about: "",
            rate: null,
            picture: 'img/sample_images/people/default-profile.jpg',
            email: "",
            worker: false
        };

        var me = User.get();

        if (me != '') {
            $state.go('app.profile.posts', {userId: me.id});
        }

        $scope.doSignUp = function () {

            $ionicLoading.show({
                template: 'Signing up...'
            });

            $scope.user.username = $scope.user.email;

            var promise = AuthService.registerUser($scope.user);
            promise.then(function (user) {

                $ionicLoading.hide();
                User.set(user.data.data[0]);

                var authUser = {
                    username: $scope.user.username,
                    password: $scope.user.password,
                    id: user.data.data[0].id
                };

                getAccessToken(authUser);

            }, function (reason) {

                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
            });

        };

        var getAccessToken = function (authUser) {

            $ionicLoading.show({
                template: 'Getting access token...'
            });

            var auth_promise = AuthService.getAuthUserId(authUser);

            auth_promise.then(function (auth_data) {
                $ionicLoading.hide();
                User.setKey('token', auth_data.data.data[0].access_token);
                $state.go('app.profile.posts', {"userId": authUser.id});

            }, function (reason) {

                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
            });

        };
    })

    .controller('WelcomeBackCtrl', function ($scope, $state, $ionicModal, AuthService, User, $ionicLoading, $ionicPopup) {

        $scope.user = {
            username: '',
            password: ''
        };

        var me = User.get();

        if (me != '') {
            $state.go('app.profile.posts', {userId: me.id});
        }

        var authData = {};

        $scope.doLogIn = function () {

            $ionicLoading.show({
                template: 'Verifying credentials...'
            });

            var auth_promise = AuthService.getAuthUserId($scope.user);

            auth_promise.then(function (auth_data) {

                $ionicLoading.hide();
                authData = auth_data.data.data[0];
                getUser(authData.user_id);

            }, function (reason) {

                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
            });

        };

        var getUser = function (userId) {

            $ionicLoading.show({
                template: 'Signing in...'
            });

            var getUser_promise = AuthService.getUser(userId);

            getUser_promise.then(function (user) {

                $ionicLoading.hide();
                User.set(user.data.data[0]);
                User.setKey('token', authData.access_token);
                $state.go('app.feed');

            }, function (reason) {

                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: reason.data != null ? reason.data.meta.error.message : reason.statusText
                });
            });
        };

        $ionicModal.fromTemplateUrl('views/auth/forgot-password.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.forgot_password_modal = modal;
        });

        $scope.showForgotPassword = function () {
            $scope.forgot_password_modal.show();
        };

        $scope.requestNewPassword = function () {
            console.log("requesting new password");
        };

        // //Cleanup the modal when we're done with it!
        // $scope.$on('$destroy', function() {
        //   $scope.modal.remove();
        // });
        // // Execute action on hide modal
        // $scope.$on('modal.hidden', function() {
        //   // Execute action
        // });
        // // Execute action on remove modal
        // $scope.$on('modal.removed', function() {
        //   // Execute action
        // });
    })

    .controller('ForgotPasswordCtrl', function ($scope) {

    })

;
