/**
 * Created by stephane on 18/03/17.
 */
'use strict'

var config = {
    'scripts': {
        'srcfolder': './www',
        'src': [
            'lib/ionic/js/ionic.bundle.min.js',
            'lib/underscore/underscore-min.js',
            'lib/moment/min/moment.min.js',
            'lib/angular-moment/angular-moment.min.js',
            'lib/knuth-shuffle/index.js',
            'lib/angular-elastic/elastic.js',
            'lib/ngCordova/dist/ng-cordova.min.js',
            'lib/angular-facebook/dist/angular-facebook.js',
            'lib/angular-facebook/dist/angular-facebook-phonegap.js',
            'lib/angular-messages/angular-messages.js',
            'lib/ngmap/build/scripts/ng-map.min.js',
            'lib/ngGeolocation/ngGeolocation.js',
            'lib/jquery.min.js',
            'lib/ng-file-upload/dist/ng-file-upload-shim.min.js',
            'lib/ng-file-upload/dist/ng-file-upload.min.js',
            'cordova.js',
            'js/app.js',
            'js/app/controllers/menuCtrl.js',
            'js/app/controllers/bookingsCtrl.js',
            'js/app/controllers/mapCtrl.js',
            'js/app/controllers/workersCtrl.js',
            'js/app/controllers/settingsCtrl.js',
            'js/app/controllers/feedCtrl.js',
            'js/app/controllers/messagesCtrl.js',
            'js/app/controllers/notificationCtrl.js',
            'js/app/controllers/bookAppointmentCtrl.js',
            'js/common/common.directives.js',
            'js/auth/auth.controllers.js',
            'js/app/app.controllers.js',
            'js/app/app.services.js',
            'js/app/app.filters.js',
            'js/views.js'
        ],
        'dest': './www/js'
    }
};

var gulp = require('gulp');
var gulpif = require('gulp-if');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var global = {
    isProd: false,
    rand: 'v1.0'
};

gulp.task('scripts', function () {

    var sources = [];
    var folder = config.scripts.srcfolder;
    for (var i = 0; i < config.scripts.src.length; i++) {
        sources.push(folder + '/' + config.scripts.src[i]);
    }
    console.log(sources);

    return gulp.src(sources)
        .pipe(concat('main-' + global.rand + '.min.js'))
        .pipe(gulpif(global.isProd, uglify()))
        .pipe(gulp.dest(config.scripts.dest))
});